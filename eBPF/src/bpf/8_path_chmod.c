#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/path.h>

#define __LOWER(x) (x & 0xffffffff)
#define __UPPER(x) (x >> 32)

CONTAINER_FILTER

/*
 * Create a data structure for collecting event data
 */
struct data_t {
    char comm[TASK_COMM_LEN];
    char pcomm[TASK_COMM_LEN];
    u32 uid;
    u32 gid;
    u32 pid;
    u32 ppid;
    unsigned short oldmode;
    unsigned short newmode;
    u32 mntns;
};

/*
 * Create a buffer for sending event data to userspace
 */
BPF_PERF_OUTPUT(events);

/*
 * Attach to the "path_chmod" LSM hook
 */
LSM_PROBE(path_chmod, const struct path *path, umode_t mode) {

    u64 gid_uid;
    u64 pid_tgid;
    int allowed;
    struct task_struct *current_task;
    struct task_struct *real_parent;
    struct nsproxy *nsproxy;
    struct mnt_namespace *mnt_ns;
    unsigned int inum;
    struct data_t data = {};
    /*
    * Filter container first 
    */
    if (!container_should_be_filtered())
        return 0;

    /*
     * Gather event data
     */
    gid_uid = bpf_get_current_uid_gid();
    pid_tgid = bpf_get_current_pid_tgid();

    bpf_get_current_comm(&data.comm, sizeof(data.comm));
    data.uid = __LOWER(gid_uid);
    data.gid = __UPPER(gid_uid);
    data.pid = __UPPER(pid_tgid);
    data.oldmode = path->dentry->d_inode->i_mode;
    data.newmode = mode;
    path->dentry->d_inode

    if(
        1
        DEFAULT_ALLOW
        
    )

    return 0;
}
