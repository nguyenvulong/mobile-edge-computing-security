######################################
# This eBPF hooking monitor and enforce
# policy to chmod system call
# LSM hook: security_path_chmod()
# Author: Phucdt
# Ref: mac_fileperms
######################################
# from ... import src.libs.container as container
from bcc import BPF
from time import sleep
import datetime

program='''
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/path.h>
#include <linux/nsproxy.h>
#include <linux/mount.h>
#include <linux/ns_common.h>
#include <linux/dcache.h>

#define __LOWER(x) (x & 0xffffffff)
#define __UPPER(x) (x >> 32)

#define MAX_LENGTH 128

/*
 * Create a data structure for collecting event data
 */
struct data_t {
    char comm[TASK_COMM_LEN];
    char pcomm[TASK_COMM_LEN];
    char name[MAX_LENGTH];
    u32 uid;
    u32 gid;
    u32 pid;
    u32 ppid;
    int oldmode;
    unsigned short newmode;
    u32 mntns;
};
struct str {
    char path[DNAME_INLINE_LEN];
    u32 len;
};

/*
 * Create a buffer for sending event data to userspace
 */
BPF_PERF_OUTPUT(events);
BPF_HASH(phuc, u32, char* , 32);


struct mnt_namespace {
    // This field was removed in https://github.com/torvalds/linux/commit/1a7b8969e664d6af328f00fe6eb7aabd61a71d13
    #if LINUX_VERSION_CODE < KERNEL_VERSION(5, 11, 0)
        atomic_t count;
    #endif
        struct ns_common ns;
};
BPF_HASH(res, u32, char);
BPF_HASH(fullpath, u32, struct str, 32);
static inline int read_dentry_path(const struct path *path, int* count){
    /* This function store path to a BPF_HASH map
    */
    struct dentry *lastdtryp;
    struct dentry *dcur;
    int full_length = 0;
    struct str test = {.len=0,.path={0}};
    // first entry
    dcur = path->dentry;
    test.len = bpf_probe_read_kernel_str(&test.path, DNAME_INLINE_LEN, dcur->d_name.name);
    full_length += test.len - 1;
    fullpath.update(count,&test);
    // NExt
    lastdtryp = dcur;
    dcur = dcur->d_parent;
    int i = 1;
    for (i = 1; i < DNAME_INLINE_LEN; i++) {
        if(lastdtryp!=dcur){
            test.len = bpf_probe_read_kernel_str(&test.path, DNAME_INLINE_LEN, dcur->d_name.name);
            full_length += test.len - 1;
            lastdtryp = dcur;
            dcur = dcur->d_parent;
            *count = i;
            fullpath.update((u32 *)count,&test);
        }
        else
            break;
    }
    *count = i-1;
    return full_length-2+i;
}

static inline int _mntns_filter() {
        struct task_struct *current_task;
        struct nsproxy *nsproxy;
        struct mnt_namespace *mnt_ns;
        unsigned int inum;

        current_task = (struct task_struct *)bpf_get_current_task();
        if (bpf_probe_read_kernel(&nsproxy, sizeof(nsproxy), &current_task->nsproxy))
            return 0;
        if (bpf_probe_read_kernel(&mnt_ns, sizeof(mnt_ns), &nsproxy->mnt_ns))
            return 0;
        if (bpf_probe_read_kernel(&inum, sizeof(inum), &mnt_ns->ns.inum))
            return 0;
        return inum == 4026532398;
}

static inline int container_should_be_filtered() {
        return _mntns_filter();
}

static inline bool equal_to_true(char *str, const char *comp, int start, int len) {
    char comparand[len];
    bpf_probe_read(&comparand, sizeof(comparand), str);
  
    for (int i = 0; i < len; ++i)
        if (comp[start+i] != comparand[i])
            return false;
    return true;
}


/*
 * Attach to the "path_chmod" LSM hook
 */

LSM_PROBE(path_chmod, const struct path *path, umode_t mode) {
    
    //if(!container_should_be_filtered())
    //    return 0;

    u64 gid_uid;
    u64 pid_tgid;
    int allowed;
    int length = 0;
    struct dentry *dtry;
    struct dentry *lastdtryp;

    struct data_t data = {};
    struct str test = {};
    int count = 0;
    gid_uid = bpf_get_current_uid_gid();
    pid_tgid = bpf_get_current_pid_tgid();


    /*
     * Gather event data
     */

    bpf_get_current_comm(&data.comm, sizeof(data.comm));
    data.uid = __LOWER(gid_uid);
    data.gid = __UPPER(gid_uid);
    data.pid = __UPPER(pid_tgid);
    
    //bpf_d_path(path, data.name, 128);

/*
    count = 0;
    dtry = path->dentry;
    test.len = bpf_probe_read_kernel_str(&test.path, DNAME_INLINE_LEN, dtry->d_name.name);
    fullpath.insert(&count,&test);


    data.mntns = read_dentry_path(path,&count);
    data.oldmode = count;
    const char comp[10] = "/usrbinid";
    char t[4] = {2,3,3,1};

*/
/*  Browse all the fullpath 
*/    
    for (int k = data.oldmode; k>=0 ;k--){
        count = k;
        struct str* tmp = fullpath.lookup(&count);
        if (tmp){
            bpf_probe_read_kernel(&data.name,DNAME_INLINE_LEN,tmp->path);
            data.newmode = tmp->len;
            events.perf_submit(ctx, &data, sizeof(data));
        }
    }
/*
    int index;
    int cur = 0;
    for (int i = 4 - 1; i>=0; i--){
        struct str* tmp;
        index=i;
        tmp = fullpath.lookup(&index);
        if (tmp){
            if(tmp->len == t[i]+1){
                if(equal_to_true(tmp->path, "comp", cur, t[i]))
                    return -1;
            }
        }
        cur = cur + t[i];
    }
*/
/*
    int index;
    struct str* tmp;
    index=0;
    tmp = fullpath.lookup(&index);
    if (tmp){
        if(tmp->len == 2+1){
            if(equal_to_true(tmp->path, comp, 7, 2))
                return -1;
        }
    }
*/
    events.perf_submit(ctx, &data, sizeof(data));
    return 0;
   
}
'''
print(BPF.support_lsm())

b = BPF(text=program)

def all_event(data, l=None):
    res = ""
    if l==None:   
        for i in dir(data):
            if (not i.startswith('_')):
                res += "{}={} ".format(i,getattr(data,i))
    else:
        for i in l:
            res += "{}={} ".format(i,getattr(data,i))
    return res

def print_event(cpu, data, size):
    """
    Print event data when a kill signal is about to be
    sent.
    """
    event = b["events"].event(data)
    print(
        datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S"), all_event(event)
    )

#setup callback function for the buffer
b["events"].open_perf_buffer(print_event)

# Poll for incoming events
while 1:
    try:
        b.perf_buffer_poll()
    except KeyboardInterrupt:
        exit()