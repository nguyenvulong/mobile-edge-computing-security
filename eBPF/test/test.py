from bcc import BPF
from time import sleep
import argparse
import datetime
import pwd
from bcc.utils import printb
import os
import sys
sys.path.insert(1, os.path.join(sys.path[0], '..'))

from src.libs import yamlrule

program='''
#include <linux/cred.h>
#include <linux/sched.h>
#include <linux/errno.h>
#include <linux/nsproxy.h>
#include <linux/mount.h>
#include <linux/ns_common.h>

#define __LOWER(x) (x & 0xffffffff)
#define __UPPER(x) (x >> 32)


struct mnt_namespace {
    // This field was removed in https://github.com/torvalds/linux/commit/1a7b8969e664d6af328f00fe6eb7aabd61a71d13
    #if LINUX_VERSION_CODE < KERNEL_VERSION(5, 11, 0)
    atomic_t count;
    #endif
    struct ns_common ns;
};

static inline int _mntns_filter() {
        struct task_struct *current_task;
        current_task = (struct task_struct *)bpf_get_current_task();
        u32 ns_id = current_task->nsproxy->mnt_ns->ns.inum;
        return ns_id;
    }
/*
 * Create a data structure for collecting event data
 */
struct data_t {
    char comm[TASK_COMM_LEN];
    char pcomm[TASK_COMM_LEN];
    u32 uid;
    u32 gid;
    u32 pid;
    u32 ppid;
    u32 mntns;
    u32 iscor;

};

/*
 * Create a buffer for sending event data to userspace
 */
BPF_PERF_OUTPUT(events);

int test(void *ctx) 
{ 
    u64 gid_uid;
    u64 pid_tgid;
    int signo;
    struct task_struct *task;

    /*
    * Gather event data
    */

    struct data_t data = {};
    gid_uid = bpf_get_current_uid_gid();
    pid_tgid = bpf_get_current_pid_tgid();
    bpf_get_current_comm(&data.comm, sizeof(data.comm));

    task = (struct task_struct *)bpf_get_current_task();
    bpf_probe_read_kernel(&data.pcomm, TASK_COMM_LEN, task->real_parent->comm);
    data.ppid = task->real_parent->tgid;

    data.uid = __LOWER(gid_uid);
    data.gid = __UPPER(gid_uid);
    data.pid = __UPPER(pid_tgid);
    //data.targetuid = p->cred->uid.val;
    //data.targetpid = p->pid;
    //data.signo = signo;

    //data.cgroupid = bpf_get_current_cgroup_id();
    data.mntns = _mntns_filter();

    data.iscor=0;
    if (data.mntns == 0xf000022e){
        data.iscor = 1;
        events.perf_submit(ctx, &data, sizeof(data));
        return -EPERM;
    }
    events.perf_submit(ctx, &data, sizeof(data));

    
    return 0; 
}
'''

b = BPF(text=program)
clone = b.get_syscall_fnname("fchmodat")
b.attach_kprobe(event=clone, fn_name="test")

# while True:
#     sleep(2)
#     s = {}
#     if len(b["phuc"].items()):
#         for k,v in b["phuc"].items():
#             s = {"ID":k.value, "count":v.value}
#         b["phuc"].clear()
#         print(s)
#     else:
#         print("No entries yet")

def print_event(cpu, data, size):
    """
    Print event data when a kill signal is about to be
    sent.
    """
    event = b["events"].event(data)
    print("%s type=chmod comm=%s pcomm=%s uid=%d gid=%d pid=%d ppid=%d mnt_ns=%x iscor=%d" % (
    	   datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S"),
           event.comm,
           event.pcomm,
           event.uid,
           event.gid,
           event.pid,
           event.ppid,
           event.mntns,
           event.iscor))


# #setup callback function for the buffer
# b["events"].open_perf_buffer(print_event)


# # Poll for incoming events
# while 1:
#     try:
#         b.perf_buffer_poll()
#     except KeyboardInterrupt:
#         exit()



print(yamlrule.load('test.yml'))
