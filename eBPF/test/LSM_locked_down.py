from bcc import BPF
from time import sleep
import datetime

program='''

#include <linux/security.h>

#include <linux/nsproxy.h>
#include <linux/mount.h>
#include <linux/ns_common.h>

#define __LOWER(x) (x & 0xffffffff)
#define __UPPER(x) (x >> 32)

/*
 * Create a data structure for collecting event data
 */
struct data_t {
    /* basic information */
    char comm[TASK_COMM_LEN];
    u32 uid;
    u32 gid;
    u32 pid;
    /* end basic information */
};

/* Container filtering */

struct mnt_namespace {
    // This field was removed in https://github.com/torvalds/linux/commit/1a7b8969e664d6af328f00fe6eb7aabd61a71d13
    #if LINUX_VERSION_CODE < KERNEL_VERSION(5, 11, 0)
        atomic_t count;
    #endif
        struct ns_common ns;
};

static inline int _mntns_filter(unsigned int mntnsid) {
        struct task_struct *current_task;
        struct nsproxy *nsproxy;
        struct mnt_namespace *mnt_ns;
        unsigned int inum;

        current_task = (struct task_struct *)bpf_get_current_task();
        if (bpf_probe_read_kernel(&nsproxy, sizeof(nsproxy), &current_task->nsproxy))
            return 0;
        if (bpf_probe_read_kernel(&mnt_ns, sizeof(mnt_ns), &nsproxy->mnt_ns))
            return 0;
        if (bpf_probe_read_kernel(&inum, sizeof(inum), &mnt_ns->ns.inum))
            return 0;
        return inum == mntnsid;
}

static inline int container_should_be_filtered(unsigned int mntnsid) {
        return _mntns_filter(mntnsid);
}

/* End Container filtering */

/*
 * Create a buffer for sending event data to userspace
 */
BPF_PERF_OUTPUT(events);
LSM_PROBE(locked_down, enum lockdown_reason what) 
{ 
    if(!container_should_be_filtered(4026532398))
        return 0;

    u64 gid_uid;
    u64 pid_tgid;
    /*
    * Gather event data
    */
    struct data_t data = {};
    gid_uid = bpf_get_current_uid_gid();
    pid_tgid = bpf_get_current_pid_tgid();
    bpf_get_current_comm(&data.comm, sizeof(data.comm));
    data.uid = __LOWER(gid_uid);
    data.gid = __UPPER(gid_uid);
    data.pid = __UPPER(pid_tgid);

    /*
    * Send data to output perf
    */
    events.perf_submit(ctx, &data, sizeof(data));

    /*
    * Condition & action
    */

    if(what != LOCKDOWN_BPF_READ) {
        return -EACCES;
    }

    return 0;
}
'''
print(BPF.support_lsm())

b = BPF(text=program)
# clone = b.get_syscall_fnname("sync")
# b.attach_kprobe(event=clone, fn_name="test")

# while True:
#     sleep(2)
#     s = {}
#     if len(b["phuc"].items()):
#         for k,v in b["phuc"].items():
#             s = {"ID":k.value, "count":v.value}
#         b["phuc"].clear()
#         print(s)
#     else:
#         print("No entries yet")

def print_event(cpu, data, size):
    """
    Print event data when a kill signal is about to be
    sent.
    """
    event = b["events"].event(data)
    print("%s type=inode_mkdir comm=%s pcomm=%s uid=%d gid=%d pid=%d cap=%0x" % (
    	   datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S"),
           event.comm,
           event.pcomm,
           event.uid,
           event.gid,
           event.pid,
           event.cap
           ))


#setup callback function for the buffer
b["events"].open_perf_buffer(print_event)


# Poll for incoming events
while 1:
    try:
        b.perf_buffer_poll()
    except KeyboardInterrupt:
        exit()