from bcc import BPF
from time import sleep
import datetime

program='''
#include <linux/cred.h>
#include <linux/sched.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/dcache.h>
#include <linux/types.h>
#include <linux/path.h>

#define __LOWER(x) (x & 0xffffffff)
#define __UPPER(x) (x >> 32)

/*
 * Create a data structure for collecting event data
 */
struct data_t {
    char comm[TASK_COMM_LEN];
    u32 uid;
    u32 gid;
    u32 pid;
    unsigned short mode;
    char name[100];

};

/*
 * Create a buffer for sending event data to userspace
 */
BPF_PERF_OUTPUT(events);


LSM_PROBE(path_mkdir, const struct path *dir, struct dentry *dentry, umode_t mode) 
{ 
    u64 gid_uid;
    u64 pid_tgid;

    /*
    * Gather event data
    */
    struct data_t data = {};
    gid_uid = bpf_get_current_uid_gid();
    pid_tgid = bpf_get_current_pid_tgid();
    bpf_get_current_comm(&data.comm, sizeof(data.comm));
    data.uid = __LOWER(gid_uid);
    data.gid = __UPPER(gid_uid);
    data.pid = __UPPER(pid_tgid);
    
    bpf_probe_read_kernel(data.name,100,dentry->d_name.name);
    
    events.perf_submit(ctx, &data, sizeof(data));
    
    return 0; 
}
'''
print(BPF.support_lsm())

b = BPF(text=program)
def print_event(cpu, data, size):
    """
    Print event data when a kill signal is about to be
    sent.
    """
    event = b["events"].event(data)
    print("%s type=inode_mkdir comm=%s uid=%d gid=%d pid=%d mode=%d dname=%s" % (
    	   datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S"),
           event.comm,
           event.uid,
           event.gid,
           event.pid,
           event.mode,
           event.name))

#setup callback function for the buffer
b["events"].open_perf_buffer(print_event)


# Poll for incoming events
while 1:
    try:
        b.perf_buffer_poll()
    except KeyboardInterrupt:
        exit()



        