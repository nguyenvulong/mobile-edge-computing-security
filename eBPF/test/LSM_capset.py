from bcc import BPF
from time import sleep
import datetime

program='''
#include <linux/sched.h>
#include <linux/capability.h>
#include <linux/cred.h>
#include <linux/security.h>

#define __LOWER(x) (x & 0xffffffff)
#define __UPPER(x) (x >> 32)

/*
 * Create a data structure for collecting event data
 */
struct data_t {
    char comm[TASK_COMM_LEN];
    char pcomm[TASK_COMM_LEN];
    u32 uid;
    u32 gid;
    u32 pid;
    u32 cap;
    
};

/*
 * Create a buffer for sending event data to userspace
 */
BPF_PERF_OUTPUT(events);


LSM_PROBE(capset, struct cred *new,
				   const struct cred *old,
				   const kernel_cap_t *effective,
				   const kernel_cap_t *inheritable,
				   const kernel_cap_t *permitted) 
{ 
    u64 gid_uid;
    u64 pid_tgid;
    struct task_struct *task;

    /*
    * Gather event data
    */
    
    struct data_t data = {};
    gid_uid = bpf_get_current_uid_gid();
    pid_tgid = bpf_get_current_pid_tgid();
    bpf_get_current_comm(&data.comm, sizeof(data.comm));
    data.uid = __LOWER(gid_uid);
    data.gid = __UPPER(gid_uid);
    data.pid = __UPPER(pid_tgid);
    data.cap = permitted->cap[0];

    /*
    * Send data to output perf
    */
    events.perf_submit(ctx, &data, sizeof(data));
    

    return 0;
}
'''
print(BPF.support_lsm())

b = BPF(text=program)
# clone = b.get_syscall_fnname("sync")
# b.attach_kprobe(event=clone, fn_name="test")

# while True:
#     sleep(2)
#     s = {}
#     if len(b["phuc"].items()):
#         for k,v in b["phuc"].items():
#             s = {"ID":k.value, "count":v.value}
#         b["phuc"].clear()
#         print(s)
#     else:
#         print("No entries yet")

def print_event(cpu, data, size):
    """
    Print event data when a kill signal is about to be
    sent.
    """
    event = b["events"].event(data)
    print("%s type=inode_mkdir comm=%s pcomm=%s uid=%d gid=%d pid=%d cap=%0x" % (
    	   datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S"),
           event.comm,
           event.pcomm,
           event.uid,
           event.gid,
           event.pid,
           event.cap
           ))


#setup callback function for the buffer
b["events"].open_perf_buffer(print_event)


# Poll for incoming events
while 1:
    try:
        b.perf_buffer_poll()
    except KeyboardInterrupt:
        exit()