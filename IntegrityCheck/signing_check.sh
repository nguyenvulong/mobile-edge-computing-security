```bash
#
# SIGNING CHECK V2 (for MEC2022)
#
# `cosign` is used to check against `harbor` registry
# container `image:tag` is verified against public key `cosign.pub`
# 
# current user must be in `docker` group or has privilege to interact with `docker` daemon
#

HARBOR_SERVER=172.16.5.111

for it in `docker images | grep $HARBOR_SERVER| awk 'BEGIN{OFS=":"}{print $1,$2}'`; do
        echo ***CHECKING IMAGE $it***  
        /home/soongsil/go/bin/cosign verify  --key cosign.pub $it
        echo
done
```
