//
// Author: Long Nguyen-Vu
// Email: longnv@soongsil.ac.kr
// Phone Number: 010-2869-1604
//

package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os/exec"
	"strings"
)

type Repository struct {
	Name []string `json:"repositories"`
}

type DockerImage struct {
	Name string   `json:"name"`
	Tags []string `json:"tags"`
}

func GetRespBody(url string) []byte {
	resp, err := http.Get(url)
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	return body
}

func CheckSign(imageName, imageTag string) ([]byte, error) {
	// Execute docker command
	// docker trust inspect localhost:5000/mariadb:self |jq '.[0].SignedTags[].Signers[]'
	// to check whether the image:tag is signed or not

	dockerCmd := strings.Join([]string{"docker trust inspect localhost:5000/", imageName,
		":", imageTag, "|jq '.[0].SignedTags[].Signers[]'"}, "")
	return exec.Command("/bin/zsh", "-c", dockerCmd).Output()
	//if err != nil  {
	//    return cmd, errors.New(err)
	//}
	//return cmd, "a"
}

func main() {
	// Docker Content Trust has two GET APIs for catalog (image names)
	// and tag list (image tags)
	// Example func GetRe
	// 	    catalog =  {"repositories":["alpine","mariadb"]}
	//		url_catalog = "http://localhost:5000/v2/_catalog"
	//  	tag list = {"name":"alpine","tags":["mine"]}
	// 		url tag list full = "http://localhost:5000/v2/alpine/tags/list"
	// For more information, visit: https://docs.docker.com/registry/spec/api/

	url_catalog := "http://localhost:5000/v2/_catalog"
	url_taglist := "http://localhost:5000/v2/"
	var checkList []string
	var di DockerImage

	// Get all repositories from url_catalog
	body := GetRespBody(url_catalog)
	var repo Repository
	json.Unmarshal([]byte(body), &repo)

	// Build a checkList from url_taglist
	for _, r := range repo.Name {
		url_taglist_full := strings.Join([]string{url_taglist, r, "/tags/list"}, "")
		checkList = append(checkList, url_taglist_full)
	}

	for _, url := range checkList {
		//fmt.Println(url)
		//body := string(GetRespBody(url))
		body := GetRespBody(url)

		json.Unmarshal([]byte(body), &di)
		//fmt.Printf("Name=%s, Tags=%s\n", di.Name, di.Tags[0])
		for i := range di.Tags {

			_, err := CheckSign(di.Name, di.Tags[i])
			if err != nil {
				fmt.Printf("Image %s:%s: No signature found!\n", di.Name, di.Tags[i])
			} else {
				fmt.Printf("Image %s:%s is Signed \n", di.Name, di.Tags[i])
			}
		}
	}
}
