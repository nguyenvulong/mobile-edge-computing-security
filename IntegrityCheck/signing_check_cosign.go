//
// Author: Long Nguyen-Vu
// Email: longnv@soongsil.ac.kr
// Phone Number: 010-2869-1604
//

//
// SIGNING CHECK V2 (for MEC2022)
//
// `cosign` is used to check against `harbor` registry
// container `image:tag` is verified against public key `cosign.pub`
//

package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os/exec"
	"strings"
	"time"
    "crypto/tls"
	"crypto/x509"
)

type HarborProject []struct {
	ChartCount         int       `json:"chart_count"`
	CreationTime       time.Time `json:"creation_time"`
	CurrentUserRoleID  int       `json:"current_user_role_id"`
	CurrentUserRoleIds []int     `json:"current_user_role_ids"`
	CveAllowlist       struct {
		CreationTime time.Time     `json:"creation_time"`
		ID           int           `json:"id"`
		Items        []interface{} `json:"items"`
		ProjectID    int           `json:"project_id"`
		UpdateTime   time.Time     `json:"update_time"`
	} `json:"cve_allowlist"`
	Metadata struct {
		Public string `json:"public"`
	} `json:"metadata"`
	Name       string    `json:"name"`
	OwnerID    int       `json:"owner_id"`
	OwnerName  string    `json:"owner_name"`
	ProjectID  int       `json:"project_id"`
	RepoCount  int       `json:"repo_count"`
	UpdateTime time.Time `json:"update_time"`
}

type HarborRepository []struct {
	ArtifactCount int       `json:"artifact_count"`
	CreationTime  time.Time `json:"creation_time"`
	ID            int       `json:"id"`
	Name          string    `json:"name"`
	ProjectID     int       `json:"project_id"`
	PullCount     int       `json:"pull_count"`
	UpdateTime    time.Time `json:"update_time"`
}

type HarborArtifact []struct {
	Accessories   interface{} `json:"accessories"`
	AdditionLinks struct {
		BuildHistory struct {
			Absolute bool   `json:"absolute"`
			Href     string `json:"href"`
		} `json:"build_history"`
	} `json:"addition_links"`
	Digest     string `json:"digest"`
	ExtraAttrs struct {
		Architecture string `json:"architecture"`
		Author       string `json:"author"`
		Config       struct {
			Cmd          []string `json:"Cmd"`
			Entrypoint   []string `json:"Entrypoint"`
			Env          []string `json:"Env"`
			ExposedPorts struct {
				Eight0TCP struct {
				} `json:"80/tcp"`
			} `json:"ExposedPorts"`
			Labels struct {
				Maintainer string `json:"maintainer"`
			} `json:"Labels"`
			StopSignal string `json:"StopSignal"`
		} `json:"config"`
		Created time.Time `json:"created"`
		Os      string    `json:"os"`
	} `json:"extra_attrs"`
	Icon              string      `json:"icon"`
	ID                int         `json:"id"`
	Labels            interface{} `json:"labels"`
	ManifestMediaType string      `json:"manifest_media_type"`
	MediaType         string      `json:"media_type"`
	ProjectID         int         `json:"project_id"`
	PullTime          time.Time   `json:"pull_time"`
	PushTime          time.Time   `json:"push_time"`
	References        interface{} `json:"references"`
	RepositoryID      int         `json:"repository_id"`
	Size              int         `json:"size"`
	Tags              []struct {
		ArtifactID   int       `json:"artifact_id"`
		ID           int       `json:"id"`
		Immutable    bool      `json:"immutable"`
		Name         string    `json:"name"`
		PullTime     time.Time `json:"pull_time"`
		PushTime     time.Time `json:"push_time"`
		RepositoryID int       `json:"repository_id"`
		Signed       bool      `json:"signed"`
	} `json:"tags"`
	Type string `json:"type"`
}

func GetRespBodyProtected(url string) []byte {
	// https://stackoverflow.com/questions/12122159/how-to-do-a-https-request-with-bad-certificate
	// https://stackoverflow.com/questions/38822764/how-to-send-a-https-request-with-a-certificate-golang

	// tr := &http.Transport{
	// 		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},}
	// client := &http.Client{Timeout: 5 * time.Second, Transport: tr}

	caCert, err := ioutil.ReadFile("/etc/docker/certs.d/core.harbor.domain/harbor-ca.crt")
    if err != nil {
        log.Fatal(err)
    }
    caCertPool := x509.NewCertPool()
    caCertPool.AppendCertsFromPEM(caCert)

    client := &http.Client{
        Transport: &http.Transport{
            TLSClientConfig: &tls.Config{
                RootCAs:      caCertPool,
            },
        },
    }

	req, err := http.NewRequest(http.MethodGet, url, http.NoBody)

	if err != nil {
		log.Fatal(err)
	}

	req.SetBasicAuth("admin", "Harbor12345")
	res, err := client.Do(req)

	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)

	return body

	// return json.NewDecoder(res.Body).Decode(target) //, target interface{}
}

func ListHarborProjects() []string {
	// curl -u admin -X 'GET' 'https://core.harbor.domain/api/v2.0/projects'
	url := "https://core.harbor.domain/api/v2.0/projects"

	// var projects Response
	var json_map HarborProject
	var projects = []string{}

	err := json.Unmarshal(GetRespBodyProtected(url), &json_map)
	if err != nil {
		log.Printf("Error happened while unarsharlling object %v\n", err)
	}

	for i := range json_map {
		projects = append(projects, json_map[i].Name)
	}
	return projects

}

func ListHarborRepositories() []string {
	// curl -u admin -X 'GET' 'https://core.harbor.domain/api/v2.0/repositories'
	url := "https://core.harbor.domain/api/v2.0/repositories"

	// var projects Response
	var json_map HarborRepository
	var repos = []string{}

	err := json.Unmarshal(GetRespBodyProtected(url), &json_map)
	if err != nil {
		log.Printf("Error happened while unarsharlling object %v\n", err)
	}

	for i := range json_map {
		repos = append(repos, json_map[i].Name)
	}
	return repos

}

func ListHarborArtifacts(proj_repo string) []string {
	// curl -u admin -X 'GET' 'https://core.harbor.domain/api/v2.0/projects/test/repositories/nginx/artifacts'
	var json_map HarborArtifact
	var tags = []string{}

	// for _, v := range ListHarborArtifacts("test", "nginx") {
	// 	log.Println(strings.Split(v, "/"))
	// 	projs = append(projs, strings.Split(v, "/")[0])
	// 	repos = append(repos, strings.Split(v, "/")[1])
	// }

	proj := strings.Split(proj_repo, "/")[0]
	repo := strings.Split(proj_repo, "/")[1]

	url := "https://core.harbor.domain/api/v2.0/projects/" + proj + "/repositories/" + repo + "/artifacts"

	err := json.Unmarshal(GetRespBodyProtected(url), &json_map)
	if err != nil {
		log.Printf("Error happened while unarsharlling object %v\n", err)
	}

	for i := range json_map[0].Tags {
		tags = append(tags, json_map[0].Tags[i].Name)
	}

	return tags

}

func CheckSign() {
	// Path to cosign (binary)
	// Path to cosign.pub (public key)
	// Path to image in the registry

	for _, pr := range ListHarborRepositories() {
		tags := ListHarborArtifacts(pr)

		for _, t := range tags {
			log.Printf("CHECKING SIGNATURE FOR: %s:%s ", pr, t)
			cmd := strings.Join([]string{"/home/longnv/.local/bin/cosign verify --key /home/longnv/cosign/test_key_1/cosign.pub core.harbor.domain/", pr, ":", t}, "")
			out, err := exec.Command("/bin/bash", "-c", cmd).CombinedOutput()
			if err != nil {
				log.Printf("INVALID SIGNATURE %s\n", err)
			}
			if err == nil {
				log.Printf("VALID SIGNATURE %s\n", out)
			}
		}
	}

}

func main() {
	CheckSign()
}
