# Dagda

> Github : https://github.com/eliasgranderubio/dagda



## Vulnerabilities database

Vulnerabilities database is updated when we install and initialize Dagda.

```python
# command initialize Dagda
watch python3 dagda/dagda/dagda.py vuln --init_status
```

The database named **vuln_database** is created follow as:

| table      | description                                                  |
| ---------- | ------------------------------------------------------------ |
| cve        | (Common Vulnerabilities and Exposure items) - source NVD NIST |
| bid        | (BugTraqs Ids items from http://www.securityfocus.com/) - source bidDB_downloader |
| exploit_db | (Offensive Security - Exploit Database) - source Offensive Security |
| rhba       | (Red Hat Bug Advisory) - source OVAL definitions for Red Hat Enterprise Linux 3 and above |
| rhsa       | (Red Hat Security Advisory) - source OVAL definitions for Red Hat Enterprise Linux 3 and above |

* Make database updates in the following files:

  **dagda/dagda/vulnDB/db_composer.py**



## Static analysis

First, make *container_id* or *image_name* for the file to be analyzed. (CLI)

and, Run the image(wanna inspect) as container.

#### 1. Get OS packages

* Get OS name from /etc/os-release file

  * <u>Obtain the OS name first</u> before getting the package list.

  ```python
  # dagda/dagda/static/os/os_info_extractor.py
  90	def get_os_name(os_release):
  91		lines = os_release.split('\n')
  92		for line in lines:
  93			if line.startswith('NAME='):
  94			return line
  ```

  * If the function above does not obtain the OS name, run Blind mode.

* Different commands are used for getting the package list depending on the OS.

| Operating system                  | Command     |
| :-------------------------------- | ----------- |
| Red Hat, CentOS, Fedora, openSUSE | rpm-aqi     |
| Debian, Ubuntu                    | dpkg -l     |
| Alpine                            | apk -v info |

##### Blind mode

if Dagda don't know the OS name exactly, Dagda is run this function.

```python
# dagda/dagda/static/os/os_info_extractor.py
104	def get_os_software_packages_blind_mode(docker_driver, container_id):
105	supported_distributions = [{'cmd': 'rpm -aqi', 'parser': parse_rpm_output_list}, {'cmd': 'dpkg -l', 'parser': parse_dpkg_output_list}, {'cmd': 'apk -v info', 'parser': parse_apk_output_list}]
108	for supported_distribution in supported_distributions:
109		packages_info = docker_driver.docker_exec(container_id, supported_distribution['cmd'], True, False)
110		if packages_info is not None and 'exec failed' not in packages_info:
111			return supported_distribution['parser'](packages_info)
```



#### 2. Get <u>malware</u> binaries in a parallel way

* Use Thread (parallel way)

  ```python
  # dagda/dagda/analysis/analyzer.py
  36	class Analyzer:
  86	malware_thread = Thread(target=Analyzer._threaded_malware, ...)
  88	malware_thread.start()
  ```

* **ClamAV** (antivirus engine) : Detecting a malware

  - Run ClamAV as container.

    ```python
    # dagda/dagada/analysis/static/av/malware_extractor.py
    33	docker_driver.docker_pull('tiredofit/clamav')
    44	docker_driver.docker_start(container_id)
    ```

* Read the result file (JSON) from ClamAV

* Save the result as a list.

  ```python
  # dagda/dagda/analysis/analyzer.py
  223 def _threaded_malware(dockerDriver, temp_dir, malware_binaries):
  228	malware_binaries.extend(malware_extractor.get_malware_included_in_docker_image(docker_driver=dockerDriver, temp_dir=temp_dir))
  ```

  

#### 3. Get <u>programming language dependencies</u> in a parallel way

* Use Thread (parallel way)

  ```python
  # dagda/dagda/analysis/analyzer.py
  36	class Analyzer:
  91	dependencies_thread = Thread(target=Analyzer._threaded_dependencies, ...)
  93	dependencies_thread.start()
  ```

* **3grander/4depcheck:0.1.0** *(OWASP dependency-check + Retire.js)*

  > Github : 
  >
  > https://github.com/eliasgranderubio/4depcheck (3grander/4depcheck)
  >
  > https://github.com/jeremylong/DependencyCheck (OWASP dependency-check)
  >
  > https://github.com/retirejs/retire.js/ (Retire.js)

  * Run 3grander/4depcheck:0.1.0 as container

    ```python
    # dagda/dagda/dep_info_extract.py
    31	docker_driver.docker_pull('3grander/4depcheck', '0.1.0')
    47	docker_driver.docker_start(container_id)
    ```

* Read the result file (JSON) from 3grander/4depcheck:0.1.0

* Save the result as list.

  ```python
  # dagda/dagda/analysis/analyzer.py
  236	def _threaded_dependencies(dockerDriver, image_name, temp_dir, dependencies):
  241	dependencies.extend(dep_info_extractor.get_dependencies_from_docker_image(docker_driver=dockerDriver, image_name=image_name, temp_dir=temp_dir))
  ```



#### 4. Waiting for the threads

* Waiting for the threads(malware, programming language dependencies)

  ```python
  # dagda/dagda/analysis/analyzer.py
  96	malware_thread.join()
  97	dependencies_thread.join()
  ```



## Generates the reports

Get the information stored in MongoDB and compare it with the results.

Only matching parts are stored in the report.

##### (1) the result of the static analysis

* os_packages
* prog_lang_dependencies
* malware_binaries

##### (2) dependencies report

* java, python, nodejs, js, ruby, php
* product, version, product_file_path, vulnerabilities, is_vulnerable, is_false_positive ...