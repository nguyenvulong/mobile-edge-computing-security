import requests
import time
import random

def main():
    WORDS = ("python", "jumble", "easy", "difficult", "answer",  "xylophone")
    word = random.choice(WORDS)
    url = "http://220.70.2.78:8080/"
    uri = url+word
    time.sleep(random.randrange(4))
    try:
        t=requests.request(method=random.choice(("POST","PUT","GET","HEAD")),url=uri)
        print(t)
    except Exception as e:
        print(e)
    

if __name__ == "__main__":
    for i in range (10000):
        main()