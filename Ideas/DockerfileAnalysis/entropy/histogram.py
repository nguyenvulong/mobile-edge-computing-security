import numpy as np
import time
from scipy.stats import entropy
from math import log, e
import pandas as pd
from monitoring import monitor

df = pd.read_csv("cve-2017-7494_samba_1.csv")
df.hist()