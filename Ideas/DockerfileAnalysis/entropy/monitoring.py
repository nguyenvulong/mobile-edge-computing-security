import json
import docker
import time
import sys
sys.path.append("..") # Adds higher directory to python modules path.
from docker_registry import LocalRegistry
from docker_image import Image
import subprocess
import time
def calculate_cpu_percent(d):
    # import json
    # du = json.dumps(d, indent=2)
    # logger.debug("XXX: %s", du)
    previous_cpu = d["precpu_stats"]["cpu_usage"]["total_usage"]
    previous_system = d["precpu_stats"]["system_cpu_usage"]
    cpu_percent = 0.0
    cpu_total = float(d["cpu_stats"]["cpu_usage"]["total_usage"])
    cpu_delta = cpu_total - previous_cpu
    cpu_system = float(d["cpu_stats"]["system_cpu_usage"])
    system_delta = cpu_system - previous_system
    online_cpus = d["cpu_stats"].get("online_cpus", len(d["cpu_stats"]["cpu_usage"]["percpu_usage"]))
    if system_delta > 0.0:
        cpu_percent = (cpu_delta / system_delta) * online_cpus * 100.0
    return cpu_percent

def calculate_mem_percent(d):
    mem_usage = float(d["memory_stats"]["usage"])
    mem_cache = float(d["memory_stats"]["stats"]["cache"])
    limit = float(d["memory_stats"]["limit"])
    return (mem_usage-mem_cache) / limit * 100.0

def graceful_chain_get(d, *args, default=None):
    t = d
    for a in args:
        try:
            t = t[a]
        except (KeyError, ValueError, TypeError, AttributeError):
            # logger.debug("can't get %r from %s", a, t)
            return default
    return t

def calculate_network_bytes(d):
    """
    :param d:
    :return: (received_bytes, transceived_bytes), ints
    """
    networks = graceful_chain_get(d, "networks")
    if not networks:
        return 0, 0
    r = 0
    t = 0
    for if_name, data in networks.items():
        # logger.debug("getting stats for interface %r", if_name)
        r += data["rx_bytes"]
        t += data["tx_bytes"]
    return r, t

class Metric():
    def __init__(self,stat,iter):
        self.iter = iter
        self.cpu = calculate_cpu_percent(stat)
        self.mem = calculate_mem_percent(stat)
        self.netr, self.nett = calculate_network_bytes(stat)
        # print("CPU % {}".format(self.cpu))
        # print("Mem % {}".format(self.mem))
        # print("Net I/O {}/{}".format(self.netr,self.nett))
    
    def __hash__(self):
        return hash(self.iter,self.cpu)
    
    def __lt__(self, obj):
        return self.iter < obj.iter

    def __eq__(self,obj):
        return self.iter == obj.iter


def monitor(name,dccom):
    # iteration
    t = 30    
    start_time = time.time()
    client = docker.from_env(timeout=300)
    # Start container
    # target.start()
    try:
        subprocess.check_output("docker-compose -f {} up -d".format(dccom), shell=True)
    except Exception as e:
        print(e)
    print("Successfully Start Container!")
    target = client.containers.list(all=True,filters={"name":name})[0]
    result = []
    min_cpu = 0.0
    max_cpu = 0.0
    min_mem = 0.0
    max_mem = 0.0
    total_cpu = 0.0
    total_mem = 0.0
    for i in range(t):
        stat = target.stats(stream=False)
        result.append(Metric(stat,i))
        max_cpu = result[-1].cpu if max_cpu < result[-1].cpu else max_cpu
        min_cpu = result[-1].cpu if min_cpu > result[-1].cpu else min_cpu
        max_mem = result[-1].mem if max_mem < result[-1].mem else max_mem
        min_mem = result[-1].mem if min_mem > result[-1].mem else min_mem
        total_cpu = total_cpu + result[-1].cpu
        total_mem = total_mem + result[-1].mem
    # Get the R/W layer
    print("commit new layer")
    target.commit(repository="daccheck",tag="checkit")
    # time.sleep(5)
    registry = LocalRegistry("127.0.0.1")
    image = Image("daccheck:checkit",registry)
    layer = image.layers[-1]
    print(layer)
    # Count file and size
    fcount, fsize = registry.count_file(image,layer)
    client.images.remove("daccheck:checkit")
    # Stop container and delete tmp
    target.stop()
    target.remove()
    avg_cpu = total_cpu/t
    avg_mem = total_mem/t
    netr = result[-1].netr
    nett = result[-1].nett
    print('{} {}'.format(fcount,fsize))

    print("Done in {}".format(time.time() - start_time))
    registry.clean_image(image)
    return min_cpu, max_cpu, avg_cpu, avg_mem, netr, nett, fcount, fsize, time.time() - start_time
    # print(json.dumps(stat,indent=4))


def main():
    # name = "cve-2017-7494_samba_1"
    # dccom = "/home/minikube/vulhub/samba/CVE-2017-7494/docker-compose.yml"
    name = "cve-2015-8562_web_1"
    dccom = "/home/minikube/vulhub/joomla/CVE-2015-8562/docker-compose.yml"
    n = 30
    resfile = open("{}_normal.csv".format(name),"a+")
    for i in range(n):
        time.sleep(5)
        print("Start the container in the {}".format(i))
        min_cpu, max_cpu, avg_cpu, avg_mem, netr, nett, fcount, fsize, t = monitor(name,dccom)
        resfile.write("{},{},{},{},{},{},{},{},{}\n".format(min_cpu, max_cpu, avg_cpu, avg_mem, netr, nett, fcount, fsize, t))
    resfile.close()

if __name__ == "__main__":
    main()