import numpy as np
import time
from scipy.stats import entropy
from math import log, e
import pandas as pd
from monitoring import monitor

def entropy(column, base=None):
    vc = pd.Series(column).value_counts(normalize=True, sort=False)
    base = e if base is None else base
    return -(vc * np.log(vc)/np.log(base)).sum()

def compare_recalEntropy(name,base,new,ep,k):
    res = 0.0
    if (new - base) > ep:
        print("Change in {} with the distance {}".format(name,new - base))
        return base, k+1
    else:
        res = (new+base)/2
        return res, k

def normalize(df,i,name,min,max):
    part = (max - min) / 3
    tmp = df.loc[i].at[name]
        # print(tmp)
    if (tmp<=min):
        df[name][i] = 1
    if (tmp>min and tmp <= min+part):
        df[name][i] = 2
    if (tmp>min and tmp <= min+2*part):
        df[name][i] = 3
    if (tmp>min and tmp <= min+3*part):
        df[name][i] = 4
    if (tmp > min+3*part):
        df[name][i] = 5
    


def main():
    name = "cve-2015-8562_web_1"
    dccom = "/home/minikube/vulhub/joomla/CVE-2015-8562/docker-compose.yml"
    df = pd.read_csv("{}_random.csv".format(name))
    df = df[:10]
    # Normalizing data
    for i in range(len(df)):
        normalize(df,i,"avg_cpu",2,8)
        normalize(df,i,"avg_mem",0.2,0.4)
        normalize(df,i,"netr",200000,500000)
        normalize(df,i,"nett",50000,200000)
        normalize(df,i,"fcount",4950,4970)
        normalize(df,i,"fsize",30360000,30380000)
    # df['avg_cpu'][0] = 1
    print(df)
    # Define threshold 
    ep_avg_cpu = 0.1
    ep_avg_mem = 0.1
    ep_netr = 0.1
    ep_nett = 0.1
    ep_fcount = 0.1
    ep_fsize = 0.1
    # Calculate base entropy
    etp_avg_cpu = entropy(df["avg_cpu"])
    etp_avg_mem = entropy(df["avg_mem"])
    etp_netr = entropy(df["netr"])
    etp_nett = entropy(df["nett"])
    etp_fcount = entropy(df["fcount"])
    etp_fsize = entropy(df["fsize"])
    print("entropy avg_cpu {}".format(etp_avg_cpu))
    print("entropy avg_mem {}".format(etp_avg_mem))
    print("entropy netr {}".format(etp_netr))
    print("entropy nett {}".format(etp_nett))
    print("entropy fcount {}".format(etp_fcount))
    print("entropy fsize {}".format(etp_fsize))
    for i in range(10):
        print("Start the container in the detection phase {}".format(i))
        min_cpu, max_cpu, avg_cpu, avg_mem, netr, nett, fcount, fsize, t = monitor(name,dccom)
        df.loc[len(df)]=[min_cpu, max_cpu, avg_cpu, avg_mem, netr, nett, fcount, fsize, t]
        normalize(df,len(df)-1,"avg_cpu",2,8)
        normalize(df,len(df)-1,"avg_mem",0.2,0.4)
        normalize(df,len(df)-1,"netr",200000,500000)
        normalize(df,len(df)-1,"nett",50000,200000)
        normalize(df,len(df)-1,"fcount",4950,4970)
        normalize(df,len(df)-1,"fsize",30360000,30380000)
        print(df)
        # calculate new entropy
        netp_avg_cpu = entropy(df["avg_cpu"])
        netp_avg_mem = entropy(df["avg_mem"])
        netp_netr = entropy(df["netr"])
        netp_nett = entropy(df["nett"])
        netp_fcount = entropy(df["fcount"])
        netp_fsize = entropy(df["fsize"])
        # compare with base entropy
        severity = 0
        etp_avg_cpu, severity =  compare_recalEntropy("AVG CPU",etp_avg_cpu,netp_avg_cpu,ep_avg_cpu,severity)
        etp_avg_mem, severity =  compare_recalEntropy("AVG MEM",etp_avg_mem,netp_avg_mem,ep_avg_mem,severity)
        etp_netr, severity =  compare_recalEntropy("NET R",etp_netr,netp_netr,ep_netr,severity)
        etp_nett, severity =  compare_recalEntropy("NET T",etp_nett,netp_nett,ep_nett,severity)
        etp_fcount, severity =  compare_recalEntropy("File Count",etp_fcount,netp_fcount,ep_fcount,severity)
        etp_fsize, severity =  compare_recalEntropy("FIle Size",etp_fsize,netp_fsize,ep_fsize,severity)

        print("Severity: {}".format(severity))
        print("New entropy avg_cpu {}".format(etp_avg_cpu))
        print("New entropy avg_mem {}".format(etp_avg_mem))
        print("New entropy netr {}".format(etp_netr))
        print("New entropy nett {}".format(etp_nett))
        print("New entropy fcount {}".format(etp_fcount))
        print("New entropy fsize {}".format(etp_fsize))
        if severity > 1:
            df = df[:len(df)-2] 
        
        time.sleep(5)
        # print (df)

if __name__ == "__main__":
    main()