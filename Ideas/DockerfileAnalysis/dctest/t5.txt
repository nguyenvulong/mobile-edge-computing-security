FROM alpine:3.11
ADD https://download.samba.org/pub/samba/stable/samba-4.6.3.tar.gz /tmp/
COPY t4.tar /tmp/
ENTRYPOINT ["echo","hello!"]