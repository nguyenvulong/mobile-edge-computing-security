FROM vulhub/ffmpeg:2.8.4-with-php
ADD file:8a9218592e5d736a05a1821a6dd38b205cdd8197c26a5aa33f6fc22fbfaa1c4d in /
CMD ["bash"]
LABEL maintainer=phithon <root@leavesongs.com>
RUN /bin/sh -c apt-get update \
    && apt-get install -y         autoconf         automake         build-essential         cmake         git-core         libass-dev         libfreetype6-dev         libsdl2-dev         libtool         libva-dev         libvdpau-dev         libvorbis-dev         libxcb1-dev         libxcb-shm0-dev         libxcb-xfixes0-dev         pkg-config         texinfo         wget         zlib1g-dev     \
    && wget -qO- https://www.ffmpeg.org/releases/ffmpeg-2.8.4.tar.gz | tar --strip-components 1 -xz -C /usr/src     \
    && cd /usr/src     \
    && ./configure --pkg-config-flags="--static" --disable-yasm     \
    && make \
    && make install     \
    && rm -rf /usr/src/*
CMD ["ffmpeg"]
LABEL maintainer=phithon <root@leavesongs.com>
RUN /bin/sh -c set -ex     \
    && apt-get update     \
    && apt-get install -y --no-install-recommends php-cli     \
    && rm -rf /var/lib/apt/lists/*

