FROM ruby:2.6.5-alpine3.10
LABEL maintainer=Rapid7
ENV APP_HOME=/usr/src/metasploit-framework
ENV NMAP_PRIVILEGED=
ENV METASPLOIT_GROUP=metasploit
RUN /bin/sh -c addgroup -S $METASPLOIT_GROUP
RUN /bin/sh -c apk add --no-cache bash sqlite-libs nmap nmap-scripts nmap-nselibs postgresql-libs python python3 ncurses libcap su-exec
RUN /bin/sh -c /usr/sbin/setcap cap_net_raw,cap_net_bind_service=+eip $(which ruby)
RUN /bin/sh -c /usr/sbin/setcap cap_net_raw,cap_net_bind_service=+eip $(which nmap)
COPY dir:e540eb2282e8d2a64b57f322ff57db7aebc602bd6bd181654dfc950723b9d52a in /usr/local/bundle
RUN /bin/sh -c chown -R root:metasploit /usr/local/bundle
COPY dir:0570c6d498fc0220debb07c5eeebc309bb219c191da443c176db78e48f4a436d in /usr/src/metasploit-framework/
RUN /bin/sh -c chown -R root:metasploit $APP_HOME/
RUN /bin/sh -c chmod 664 $APP_HOME/Gemfile.lock
RUN /bin/sh -c cp -f $APP_HOME/docker/database.yml $APP_HOME/config/database.yml
WORKDIR /usr/src/metasploit-framework
ENTRYPOINT ["docker/entrypoint.sh"]
CMD ["./msfconsole" "-r" "docker/msfconsole.rc" "-y" "$APP_HOME/config/database.yml"]
