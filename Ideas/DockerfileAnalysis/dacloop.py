import time
import subprocess
from dacparser import LoopParser
import json
import os

listfile = []
with open("/home/minikube/dockerfileanalysis/dockerhub/image-names.txt","r") as f:
    for i in f.readlines():
        listfile.append(i.strip())
timeRes = open("timeresdac_new.csv","a+")
for i in range(2000,2500):
    f = listfile[i]
    print("**************************************************")
    print("Scanning {}".format(f))
    print("Removing {}".format(listfile[i-1]))
    # remove last image
    try:
        out = subprocess.check_output("docker rmi $(docker images -f \"dangling=true\" -q)",shell=True,timeout=600)
        print(out)
    except Exception as e:
        print (e)
        pass
    try:
        out = subprocess.check_output("docker image rm {}:latest".format(listfile[i-1]),shell=True,timeout=600)
        print(out)
    except Exception as e:
        print (e)
        pass
    
    # Pull image to scan:
    print("************PULL IMAGE**************")
    try:
        out = subprocess.check_output("docker pull {}:latest".format(f),shell=True,timeout=600)
        print(out)
    except Exception as e:
        print (e)
        continue

    dacfile = "./dacresult/dac-{}_latest.json".format(f.replace("/","_").replace(":","_"))
    clairfile ="./clairresult/clair-{}_latest.json".format(f.replace("/","_").replace(":","_"))
    if (not os.path.isfile(dacfile)):
        # scan for this if not yet scanned :)))
        print("************DAC SCANNING**************")
        image = "{}:latest".format(f)
        target = "whatever"
        try:
            dac = LoopParser(image,target)
            start_time = time.time()
            res = dac.parse_file()
            print("Total time: {}".format(time.time()-start_time))
            timeRes.write("dac,{},{}\n".format(f,time.time()-start_time))
        except Exception as e:
            print(e)
            continue
    
    if (not os.path.isfile(clairfile)):
        print("************CLAIR SCANNING**************")
        # using clair if not scanned yet ((:
        start_time = time.time()
        outfile="./clairresult/clair-{}_latest.json".format(f.replace("/","_"))
        if os.path.isfile(outfile):
            os.remove(outfile)
        try:
            out=subprocess.check_output("claircli -c http://172.17.0.1:6060 -l 172.17.0.1 -f json -d {}:latest".format(f),shell=True)
            # print(out)
        except Exception as e:
            print("Error in image: {} {}".format(f,e))
            continue
        print("Total time: {}".format(time.time()-start_time))
        timeRes.write("clair,{},{}\n".format(f,time.time()-start_time))
        _result = []
        result = outfile
        count = 0
        if os.path.isfile(result):
            with open(result) as json_file:
                data = json.load(json_file)["Vulnerabilities"]
                    # parse data
                for p in data:
                    tmp = {"package":p["FeatureName"],"version":p['FeatureVersion'],"cve":p['Name'],"severity":p['Severity']}
                    _result.append(tmp)
                    if ("nginx" in tmp["package"]):
                        print(tmp["cve"])
                        count = count +1
        print("Image: {}\n".format(f))
        print("Total CVE: {}".format(len(_result)))
        print("Total target CVE: {}".format(count))

    # remove the image

timeRes.close()
