# -*- coding: utf-8 -*-
import logging
import os
from pprint import pformat
from stat import S_ISREG, S_ISDIR
from requests import request

logger = logging.getLogger(__name__)

class File():
    def __init__(self, fullPath, prefix):
        if (len(prefix)>3):  
            self.path = fullPath.replace(prefix,"")
        else:     
            self.path = fullPath
        try:
            self.permission = oct(os.stat(fullPath).st_mode)[-3:]
        except:
            self.permission = '000'

    def __str__(self):
        return "path: {}\t permission: {}".format(self.path,self.permission)

def mkpdirs(path):
    if not os.path.isdir(path):
        os.makedirs(path)


def request_and_check(method, url, **kwargs):
    logger.debug('%s: %s with parameters: %s', method, url, pformat(kwargs))
    resp = request(method, url, **kwargs)
    logger.debug('Response: %s', resp)
    resp.raise_for_status()
    return resp

def getListOfFiles(dirName,prefix):
    # create a list of file and sub directories 
    # names in the given directory 
    try:
        listOfFile = os.listdir(dirName)
    except Exception as e:
        print(e)
        listOfFile = []
    allFiles = list()
    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = os.path.join(dirName, entry)
        try:
            st = os.lstat(fullPath)
        except EnvironmentError:
            # print ("EnvironmentError")
            continue
        else:
            if S_ISREG(st.st_mode) or S_ISDIR(st.st_mode):
                tmp = File(fullPath,prefix)
                # If entry is a directory then get the list of files in this directory 
                if os.path.isdir(fullPath):
                    allFiles = allFiles + getListOfFiles(fullPath,prefix)
                else:
                    # print("appended")
                    allFiles.append(tmp)    
    return allFiles        