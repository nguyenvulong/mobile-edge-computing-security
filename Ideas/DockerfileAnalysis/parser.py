from argparse import ArgumentParser
import logging
import sys
import argparse
import json
import colorlog
from dockerfile_parse import DockerfileParser
import re
import os
from docker_registry import LocalRegistry
from docker_image import Image
logger = logging.getLogger(__name__)


class Intruction():
    
    _instruction = ""
    _args=""
    _isLayer = False
    _layer = ""
    _fileList = []
    def __init__(self,line):
        self._instruction = line['instruction']
        self._args = line['value']
        self.setup()
    def setup(self):
        logger.debug(self._instruction)
        logger.debug(self._args)
    
    def setFileList(self,fileList):
        if self._isLayer:
            self._fileList = fileList
    
    # def getExecList(self):
        

class CMD(Intruction):
    def setup(self):
        # self._instruction = "CMD"
        cmd = self._args
        cmd = cmd.replace("[","").replace("\\\\","\\").replace("\\\"","\"").replace("]","")
        if ('/bin/sh" "-c" "' in cmd):
            _args = cmd.replace('/bin/sh" "-c" "',"")[1:-1].split(" ")
        else:
            _args = cmd.replace("\"","").split(" ")
        self._args = _args
        logger.debug(self._instruction)
        logger.debug(self._args)
        # self.bin=_args[0]
        # self._args=_args[1:]

class ENTRYPOINT(Intruction):
    def setup(self):
        # self._instruction = "ENTRYPOINT"
        cmd = self._args
        cmd = cmd.replace("[","").replace("\\\\","\\").replace("\\\"","\"").replace("]","")
        if ('/bin/sh" "-c" "' in cmd):
            _args = cmd.replace('/bin/sh" "-c" "',"")[1:-1].split(" ")
        else:
            _args = cmd.replace("\"","").split(" ")
        self.bin=_args[0]
        self._args=_args[1:]
        logger.debug(self._instruction)
        logger.debug(self.bin)
        logger.debug(self._args)

class RUN(Intruction):
    download_re="(wget|curl).*(http|ftp|https)://([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?"
    def setup(self):
        self._isLayer = True
        # self._instruction = "RUN"
        self.cmds=self._args.replace("|","&&").replace(";","&&").split("&&")
        self.urls=re.findall(self.download_re,"\n".join(self.cmds))
        logger.debug(self._instruction)
        logger.debug(self.urls)

class ADD(Intruction):
    def setup(self):
        self._isLayer = True
        # self._instruction = "ADD"
        _file = self._args.split(" ")[-1]
        self._dst = self._args.split(" ")[-1]
        head, tail = os.path.split(_file)
        if tail:
            self._type="file"
        else:
            self._type="dir"
        logger.debug(self._instruction)
        logger.debug(self._dst)
        logger.debug(self._type)
    
    def setFileList(self,fileList):
        if self._type == "file":
            self._fileList = [self._dst]
        else:
            super().setFileList(fileList)

class FROM(Intruction):
    def setup(self):
        self._isLayer = True
        self.baseOS = self._args
        logger.debug(self._instruction)
        logger.debug(self.baseOS)
    
    def setFileList(self,fileList):
        # DO NOT NEED to set file list for FROM Instruction
        self._fileList = []

class Parser():
    description = """
    Dockerfile Parser
    """
    def __init__(self):
        # common options
        opt = ArgumentParser(
            description=self.description,
            formatter_class=argparse.RawDescriptionHelpFormatter
        )

        opt.add_argument(
            "-f","--file",
            help="Dockerfile that wanna parse"
        )
        opt.add_argument("-i","--image",
            help="Docker image name that match with the given dockerfile")
        opt.add_argument('-L', '--log-file', help='save log to file')
        opt.add_argument(
            '-d', '--debug', action='store_true', help='print more logs')

        opt.set_defaults(func=self.parse_file)
        self.args = opt.parse_args()
        self.setup_logging()
    
    def setup_logging(self):
        # logger = logging.getLogger('parser')
        console_formatter = colorlog.ColoredFormatter(
            '%(log_color)s%(asctime)s|%(levelname)s|%(message)s')
        file_formatter = logging.Formatter(
            '%(asctime)s|%(levelname)s| %(message)s')
        stdout = colorlog.StreamHandler(sys.stdout)
        stdout.setFormatter(console_formatter)
        logger.addHandler(stdout)
        logger.setLevel(logging.INFO)
        if self.args.debug:
            logger.setLevel(logging.DEBUG)
        if self.args.log_file:
            handler = logging.FileHandler(self.args.log_file, 'w', delay=True)
            handler.setFormatter(file_formatter)
            logger.addHandler(handler)

    def parse_file(self):
        
        registry = LocalRegistry("127.0.0.1")
        image = Image(self.args.image,registry)
        layers = image.layers
        dfp = DockerfileParser()
        if (not self.args.file):
            dfp.content = "\n".join(image.dockerfile)
        else:
            with open(self.args.file,'r',encoding='utf-8') as f:
                dfp.content = f.read()
        intructions = []
        for line in dfp.structure:
            if (line['instruction']=="ADD" or line['instruction']=="COPY"):
                intructions.append(ADD(line))
            elif (line['instruction']=="RUN"):
                intructions.append(RUN(line))
            elif (line['instruction']=="CMD"):
                intructions.append(CMD(line))
            elif (line['instruction']=="ENTRYPOINT"):
                intructions.append(ENTRYPOINT(line))
            elif (line['instruction']=="FROM"):
                intructions.append(FROM(line))
            else:
                intructions.append(Intruction(line))
    

        # TODO
        i = 0
        for intr in intructions:
            if (intr._isLayer):
                logger.debug("Instruction {} mapping ...".format(intr._instruction))
                intr._layer = layers[i]
                intr.setFileList(registry.get_file_list(image,layers[i]))
                i=i+1


        # logging
        for intr in intructions:
            logger.info("Intrucion: {} map with Layer: {}".format(intr._instruction,intr._layer))
            logger.warning("File list: {}".format(intr._fileList))

        # print(image.manifest)
        # print(registry.get_file_list(image,layers[2],isFull=True))
        image.clean()



    def run(self):
        return self.args.func()


def main():
    cli = Parser()
    sys.exit(cli.run())


if __name__ == "__main__":
    main()

