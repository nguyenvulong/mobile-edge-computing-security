import json
import os
class Result():
    package = ""
    version = ""
    cve = ""
    severity = ""
    def __init__(self,package,version,cve_number,severity):
        self.package = package
        self.version = version
        self.cve = cve_number
        self.severity = severity

    def __eq__(self, obj):
        return self.cve.upper() == obj.cve.upper()

    def __str__(self):
        return "{}".format(self.cve.upper())

    def __hash__(self):
        return hash(self.cve)
listfile = []
with open("/home/minikube/dockerfileanalysis/dockerhub/image-names.txt","r") as f:
    for i in f.readlines():
        listfile.append(i.strip())

compareres = open("/home/minikube/onlyDAC.csv","w")
# dac=[]
for f in listfile:
    dacfile = "./dacresult/dac-{}_latest.json".format(f.replace("/","_").replace(":","_"))
    clairfile ="./clairresult/clair-{}_latest.json".format(f.replace("/","_").replace(":","_"))
    if (os.path.isfile(dacfile) and (not os.path.isfile(clairfile))):
        with open(dacfile) as json_file:
            data = json.load(json_file)
                # parse data
            compareres.write("{},{}\n".format(f,len(data)))


                
    # clairfile ="./clairresult/clair-{}_latest.json".format(f.replace("/","_").replace(":","_"))
    # if (os.path.isfile(dacfile) and os.path.isfile(clairfile)):
    #     dac = []
    #     clair = []
    #     with open(clairfile) as json_file:
    #         data = json.load(json_file)["Vulnerabilities"]
    #         # parse data
    #         for p in data:
    #             # tmp = {"package":p["FeatureName"],"version":p['FeatureVersion'],"cve":p['Name'],"severity":p['Severity']}
    #             tmp = Result(p["FeatureName"],p['FeatureVersion'],p['Name'],p['Severity'])
    #             clair.append(tmp)

    #     with open(dacfile) as json_file:
    #         data = json.load(json_file)
    #             # parse data
    #         for p in data:
    #             tmp = Result(p["package"],p['version'],p['cve'],p['severity'])
    #             # tmp = {"package":p["package"],"version":p['version'],"cve":p['cve'],"severity":p['severity']}
    #             dac.append(tmp)
    #             # dac.append({"package":p["package"],"version":p['version'],"cve":p['cve'],"severity":p['severity']})
    #     samelist = []
    #     dac = list(dict.fromkeys(dac))
    #     for i in dac:
    #         for j in clair:
    #             if i==j:
    #                 samelist.append(i)
        
    #     print("{},{},{},{},{},{}".format(f,len(samelist),len(dac),len(clair),len(dac)-len(samelist),len(clair)-len(samelist)))
    #     compareres.write("{},{},{},{},{},{}\n".format(f,len(samelist),len(dac),len(clair),len(dac)-len(samelist),len(clair)-len(samelist)))



compareres.close()