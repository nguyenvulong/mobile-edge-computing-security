import time
import subprocess
import os
import json
from collections import Counter

listfile = []
with open("/home/minikube/dockerfileanalysis/dockerhub/image-names.txt","r") as f:
    for i in f.readlines():
        listfile.append(i.strip())
timeRes = open("timeres_100_200.csv","a+")
for f in listfile[100:200]:
    # try:
    #     subprocess.check_output("docker pull {}:latest".format(f),shell=True)
    # except Exception as e:
    #     print (e)
    #     continue
    start_time = time.time()
    outfile="clair-{}_latest.json".format(f.replace("/","_"))
    if os.path.isfile(outfile):
        os.remove(outfile)
    try:
        out=subprocess.check_output("claircli -c http://172.17.0.1:6060 -l 172.17.0.1 -f json -d {}:latest".format(f),shell=True)
        # print(out)
    except Exception as e:
        print("Error in image: {} {}".format(f,e))
        pass
    print("Total time: {}".format(time.time()-start_time))
    timeRes.write("{},{}\n".format(f,time.time()-start_time))
    _result = []
    result = outfile
    count = 0
    if os.path.isfile(result):
        with open(result) as json_file:
            data = json.load(json_file)["Vulnerabilities"]
                # parse data
            for p in data:
                tmp = {"package":p["FeatureName"],"version":p['FeatureVersion'],"cve":p['Name'],"severity":p['Severity']}
                _result.append(tmp)
                if ("nginx" in tmp["package"]):
                    print(tmp["cve"])
                    count = count +1
    print("Image: {}\n".format(f))
    print("Total CVE: {}".format(len(_result)))
    print("Total target CVE: {}".format(count))
timeRes.close()