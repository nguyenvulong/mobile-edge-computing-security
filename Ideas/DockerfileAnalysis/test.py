import os
import subprocess
import json
import time
from collections import Counter
class Result():
    package = ""
    version = ""
    cve = ""
    severity = ""
    def __init__(self,package,version,cve_number,severity):
        self.package = package
        self.version = version
        self.cve = cve_number
        self.severity = severity
start_time = time.time()
outfile="samba.json"
if os.path.isfile(outfile):
    os.remove(outfile)
out=subprocess.check_output("cve-bin-tool {} -f json -o {} -x".format("/home/minikube/dockerfileanalysis/dctest/sambaimage/",outfile),shell=True)
print(out)
print("Total time: {}".format(time.time()-start_time))
_result = []
result = outfile
count = 0
if os.path.isfile(result):
    with open(result) as json_file:
        data = json.load(json_file)
            # parse data
        for p in data:
            if ("samba" in p["product"]):
                print(p['cve_number'])
                count = count +1
            _result.append(Result(p["product"],p['version'],p['cve_number'],p['severity']))

print("Total CVE: {}".format(len(_result)))
print("Total target CVE: {}".format(count))